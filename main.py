import os
from pathlib import Path
from pdfminer.high_level import extract_text


out_directory = Path(__file__).resolve().parent 
text_file = os.path.join(out_directory, 'pdfminer2.txt')


def extract_text_from_pdf():
    with open('DocumentacionModeladoFInal.pdf', 'rb') as f:
        text = extract_text(f)
    return text


def write_file_txt(text):
    with open(text_file, "a") as output_file:
        output_file.write(text)


if __name__ == "__main__":    
    text = extract_text_from_pdf()
    write_file_txt(text)
