Accidentalidad Vial 2017 - 2021
Eventos de accidentalidad registrados en las vías administradas por

INVIAS

DOCUMENTACIÓN
Análisis de Fuente
Identificación de Dimensiones

EQUIPO DIMENSIONAMIENTO
Ronald Paternina  - Steeven Altamiranda - Amanda Quintana
Andres García - Jorge Zetien - Zuleydis Barrios

1

IDENTIFICACIÓN DE DIMENSIONES

Se inició construyendo una tabla de hecho para tener un punto de partida para el
dimensionamiento de los datos suministrados una vez construida las tabla de hecho se
procedió a crear las dimensiones para que los datos presentados permitan un análisis
adecuado.

Dimensión: Territorio
Esta dimensión provee información del departamento en el que ocurre un accidente para
tener un sesgo territorial .

Campos:

● id_territorio:

Identificador único autoincrementable para cada territorio o

departamento.

● territorial: Representa la región o departamento donde se presenta el accidente.

Dimensión: condición meteorológica
Esta dimensión nos muestra el clima o condición climatológica que se presentó durante el
lugar del accidente, es fundamental para determinar de qué forma influye esta condición con
la accidentalidad.

Campos:

● id_cond_meteor:
meteorológica.

identificador único autoincrementable para cada condición

● condic_meteor: este campo corresponde al tipo de condición meteorológica que se

presentaba en el lugar al momento del accidente.

Dimensión: Calendar
en esta dimensión se busca generar un calendario con de fechas específicas que no
presentan una relación directa con la tabla de hechos, pero es útil para presentar de manera
ordenada una correlación entre los accidentes y la fecha en la que estos ocurren, de modo
que sea más representativo un calendario organizado de estos accidentes.

Campos:

● id_calendar:
específico.

identificador único autoincrementable para un valor calendario

● date_actual: este campo hace referencia a la fecha actual del calendario.
● day_name: en este campo se define el nombre del día respectivo de esa fecha.
● day_of_week: este campo corresponde al día exacto de la semana del 1 al 7.
● day_of_month: este campo corresponde al día del mes de dicha fecha.
● week_of_month: este campo hace referencia a la semana del mes que va de 1 a 4.
● week_of_year: este campo hace referencia a la semana del año que va de 1 a 48.
● month_name: el es nombre del mes de dicha fecha.

2

● month_name_abbreviated: éste campo corresponde a la abreviación del nombre

del mes.

● first_day_of_week: este campo hace referencia a la fecha del primer día de la

semana actual.

● last_day_of_week: este campo hace referencia a la fecha del ultimo dia de la

semana actual.

● first_day_of_month: este campo hace referencia a la fecha del primer día del mes

actual.

● last_day_of_month: este campo hace referencia a la fecha del último día del mes

actual.

● first_day_of_year: este campo hace referencia a la fecha del primer día del año

actual.

● last_day_of_year: este campo hace referencia a la fecha del último día del año

actual.

● mmyyyy: este campo es una forma ordenada de ver en un texto el mes y el año

actual en formato mmaaaa.

● mmddyyyy: este campo es una forma ordenada de ver en un texto el mes, día y

año actual en un formato mmddaaaa.

● weekend_indr: este campo nos permite identificar si el día de la fecha actual es un

un fin de semana o no.

Dimension: Lugar Accidente:
Esta dimensión define y establece el lugar específico en el cual se presentó el accidente
vial, describiendo a detalle el territorio, ubicación, y referencias.

Campos:

● id_lugar_accidente: identificador único autoincrementable que hace referencia al

lugar donde se presentó el accidente.

● id_territorio: hace referencia a la información territorial en la que ocurrió el

accidente vial.

● ruta_id: es la identificación de la vía, valor que también está relacionado con el

territorio y el amv.

● codigo_via: es un código único  que hace referencia al territorio y al amv específico.
● amv: Representa el sector o segmento del territorio donde ocurrió el accidente con

las iniciales del departamento y un número entero.

● secc_tip: hace referencia al tipo de talud en el cual ocurrió el siniestro.

Dimensión: Estado Carretera
Al observar los datos se concluyó una extensión en esta dimensión era necesaria, razón por
la que la se tiene una conexión con la condición meteorológica. La dimensión responde a la
pregunta ¿Cómo estaba la carretera? Dando información del terreno, el clima y el estado de
la carretera.

Campos:

● id_estado_carretera: identificador único autoincrementable que hace referencia al

estado de la carretera al momento del accidente.

3

● id_condic_meteor: Campo tipo llave foránea, que permite relacionar a la carretera

una condición meteorológica específica.
estado_super: Campo tipo varchar que da información del estado temporal de la
carretera al momento del accidente.

● Terreno: Campo tipo varchar que da información de las deformaciones naturales

que tiene la carretera.

Dimensión: Clase de accidente
Con esta dimensión obtenemos la información sobre el tipo exacto de accidente que ocurrió,
de esta forma podemos clasificar los accidentes que se presentan por las diferentes clases.

Campos:

● id_clase_accidente: identificador único autoincrementable que hace referencia al

tipo de accidente presentado.

● clase_accidente: este campo hace referencia al

tipo o clase de accidente

específico.

Dimensión: Causa old
Con esta dimensión se planea identificar qué evento pudo haber influenciado en que el
accidente se presentará, y de esta forma clasificar las
causas probables de dicho
accidente.

Campos:

● id_causa_old: identificador único autoincrementable que hace referencia a la causa

probable del accidente.

● causa_old: este campo hace referencia a la causa o motivo probable del accidente.

Dimensión: Causa posible
Con esta dimensión clasificar las causas posibles del accidente desde el punto de vista de
quien realizó el reporte de este, está vinculada con la dimensión de causa old, pero se tiene
cierta discrepancia en alguno de estas causas.

Campos:

● id_causa_posible: identificador único autoincrementable que hace referencia a la

causa posible del accidente según quien realizó el reporte.

● causa_old: este campo hace referencia a la causa posible del accidente dicho por

quien presenta el reporte.

4

Campos descartados

A continuación se presentan campos descartados de la tabla original, dichos campos no
cumplen requisitos para ser considerados de la tabla de hecho por diversas razones,
principalmente debido a que la información presentada no es diciente o hay pocos registros
como para establecer relevancia en el campo.

● object_id: Campo tipo Primary key, es la llave primaria original de la tabla usada
para construcción del data-warehouse, sim embargo este campo es auto generado
por la base de datos que se utilice al procesar la información.

● codigo: Campo tipo integer, del cual no se especifica el uso de este código, ni se

pudo establecer una relación entre este campo y los demás datos.

● pr: Campo tipo integer, genera un dígito no mayor a 3 caracteres, expresa un

sistema de medida, sin embargo no extrae una medida clara.

● distancia_pr: Campo tipo integer, expresa una relación con el campo pr, sin
embargo al no encontrar utilidad a directa a dicho campo, esta subdivisión pierde
importancia.

● ref_met: Campo tipo varchar, presenta un punto de referencia único, por lo que su

información no aporta ningún valor.

● ref_loc: Campo tipo varchar, expresa valores combinados del codigo_via y pr, por lo

que el campo en sí mismo es información redundante.

● ref_off: Campo tipo integer, es un duplicado del campo distancia_pr.
● to_date: Campo tipo null, no presenta registro de ningún tipo.
● from_date: Campo tipo datetime: expresa fechas de registro que no coinciden con

las fechas de los accidentes que se presentan en el campo fecha_acc

● meas: Campo tipo float, expresa un número de varios dígitos del cual no se pudo

extraer información de interés.

● eventid: Campo tipo varchar, expresa una relación con el código, del cual se

comentó previamente porqué se descartó.

5

MODELO DE  DIMENSIONES

6

DOCUMENTACIÓN CREACIÓN DE SCRIPTS

PASO 1 CREACIÓN DE BASE DE DATOS ACCIDENTALIDAD 2017-2021

-- Database: accidentalidad-2017-2021
-- DROP DATABASE IF EXISTS "accidentalidad-2017-2021";
CREATE DATABASE "accidentalidad-2017-2021"

PASO 2 CREACIÓN DE TABLA TERRITORIO

CREATE TABLE public.territorio
(

id_territorio serial NOT NULL,
territorial character varying(100),
PRIMARY KEY (id_territorio)

);

ALTER TABLE IF EXISTS public.territorio

OWNER to postgres;

COMMIT;

7

PASO 3 CREACIÓN TABLA CONDICION_METEREOLOGICA

CREATE TABLE public.condicion_metereologica
(

id_cond_meteor serial NOT NULL,
condic_meteror character varying(100),
PRIMARY KEY (id_cond_meteor)Mo

);

ALTER TABLE IF EXISTS public.condicion_metereologica

OWNER to postgres;

COMMIT;

8

PASO 4 CREACIÓN DE TABLA CALENDAR

DROP TABLE if exists calendar;

CREATE TABLE calendar
(

id_calendar
date_actual
day_name
COMMIT;
day_of_week
day_of_month
week_of_month
week_of_year
month_name

INT NOT NULL,
DATE NOT NULL,
VARCHAR(9) NOT NULL,

INT NOT NULL,
INT NOT NULL,
INT NOT NULL,
INT NOT NULL,
VARCHAR(9) NOT NULL,

9

month_name_abbreviated   CHAR(3) NOT NULL,
ﬁrst_day_of_week
last_day_of_week
ﬁrst_day_of_month
last_day_of_month
ﬁrst_day_of_year
last_day_of_year
mmyyyy
mmddyyyy
weekend_indr

DATE NOT NULL,
DATE NOT NULL,
DATE NOT NULL,
DATE NOT NULL,
DATE NOT NULL,
DATE NOT NULL,
CHAR(6) NOT NULL,
CHAR(10) NOT NULL,
BOOLEAN NOT NULL

);

ALTER TABLE public.calendar ADD CONSTRAINT calendar_id_calendar_pk PRIMARY KEY
(id_calendar);

CREATE INDEX calendar_date_actual_idx

ON calendar(date_actual);

COMMIT;

PASO 5 CREACIÓN TABLA LUGAR_ACCIDENTE

CREATE TABLE public.lugar_accidente
(

id_lugar_accidente serial NOT NULL,
id_territorio integer,
ruta_id character varying(100),
codigo_via character varying(100),
amv character varying(100),

10

secc_tip character varying(100),
PRIMARY KEY (id_lugar_accidente),
CONSTRAINT id_territorio FOREIGN KEY (id_territorio)
REFERENCES public.territorio (id_territorio) MATCH SIMPLE
ON UPDATE NO ACTION
ON DELETE NO ACTION
NOT VALID

);

ALTER TABLE IF EXISTS public.lugar_accidente

OWNER to postgres;

COMMIT;

PASO 6 CREACIÓN ESTADO_CARRETERA

CREATE TABLE public.estado_carretera
(

id_estado_carretera serial NOT NULL,
id_condic_meteor integer,
condicion_metereologica.id_condic_meteor
estado_super character varying(100),
terreno character varying(100),
PRIMARY KEY (id_estado_carretera),
CONSTRAINT id_condic_meteor FOREIGN KEY (id_condic_meteor)
REFERENCES public.condicion_metereologica (id_cond_meteor) MATCH SIMPLE
ON UPDATE NO ACTION
ON DELETE NO ACTION
NOT VALID

11

);

ALTER TABLE IF EXISTS public.estado_carretera

OWNER to postgres;

COMMIT;

PASO 7 CREACIÓN TABLA CLASE_DE_ACCIDENTES

CREATE TABLE public.clase_de_accidentes
(

id_clase_accidente serial NOT NULL,
clase_accidente character varying(100),
PRIMARY KEY (id_clase_accidente)

);

ALTER TABLE IF EXISTS public.clase_de_accidentes

OWNER to postgres;

COMMIT;

12

PASO 8 CREACIÓN TABLA CAUSA_OLD

CREATE TABLE public.causa_old
(

id_causa_old serial NOT NULL,
causa_old character varying,
PRIMARY KEY (id_causa_old)

);

ALTER TABLE IF EXISTS public.causa_old

OWNER to postgres;

COMMIT;

13

PASO 9 CREACIÓN TABLA CAUSA_POSIBLE

CREATE TABLE public.causa_posible
(

id_causa_posible serial NOT NULL,
causa_posible character varying(100),
PRIMARY KEY (id_causa_posible)

);

ALTER TABLE IF EXISTS public.causa_posible

OWNER to postgres;

COMMIT;

14

PASO 10 CREACIÓN TABLA ACCIDENTE

CREATE TABLE public.clase_de_accidentes
(

id_clase_accidente serial NOT NULL,
clase_accidente character varying(100),
PRIMARY KEY (id_clase_accidente)

);

ALTER TABLE IF EXISTS public.clase_de_accidentes

OWNER to postgres;

COMMIT;

15

16

